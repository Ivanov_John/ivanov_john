package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.EmptyStackException;
import java.util.Stack;
import java.util.TreeMap;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // если пустое значение
        if (statement==null ||statement.isEmpty()){
            return null;
        }

        // Используем Алгоритм сортировочной станции(алгоритм Дейкстры с двумя стеками), учитывающий порядок операции
        Stack<String> operation = new Stack<String>();
        Stack<Double> values = new Stack<Double>();
        StringBuilder buffer = new StringBuilder(); // зачитываем числа состоящие из нескольких символов

        try {
            for (int i = 0; i < statement.length(); i++) {
                // читаем строку пока не получим операцию, числа записываем в буфер
                String s = String.valueOf(statement.charAt(i));
                if (!operationPriority.containsKey(s)) {
                    buffer.append(s);
                    continue;
                }

                // обнуляем буфер, записываем числа в стек
                if (buffer.length() != 0){
                    values.push(Double.parseDouble(buffer.toString()));
                    buffer.setLength(0);
                }

                // читаем оператор
                while (true) {

                    // операции с большим приоритетом исполняются первыми
                    if (operation.isEmpty() || s.equals("(") || (operationPriority.get(s) > operationPriority.get(operation.peek()))) {
                        operation.push(s);
                        break;
                    }

                    // вычисляем выражение
                    String op = operation.pop();

                    // пропускаем открывающие скобки
                    if (op.equals("(")) {
                        if (!s.equals(")")) {
                            return null; // отсутствует закрывающая скобка
                        }
                        break;
                    } else {
                        // вычисляем значение операции и двух значений, результат в стек
                        double val2 = values.pop();
                        double val1 = values.pop();
                        values.push(eval(op, val1, val2));
                    }
                }
            }

            // достаем из буфера последнее значение
            if (buffer.length() > 0) {
                values.push(Double.parseDouble(buffer.toString()));
            }

            // строка разобрана, вычисляем значения из стека
            while (!operation.isEmpty()) {
                String op = operation.pop();
                double val2 = values.pop();
                double val1 = values.pop();
                values.push(eval(op, val1, val2));
            }

            // получаем результат вычислений
            Double result = values.pop();

            // если в стеке остались значения исходное выражение было не верно
            if (!values.isEmpty()) {
                return null;
            }

            // проверяем вычисленное значение
            if (result.isInfinite() || result.isNaN()){
                return null;
            }

            // приводим тип результата
            if ((result % 1) == 0) {
                return String.valueOf(result.longValue());
            } else {
                // округлим значение
                NumberFormat numberFormat = NumberFormat.getInstance();
                numberFormat.setMaximumFractionDigits(4);
                return numberFormat.format(result).replace(",", ".");
            }
        } catch (IllegalArgumentException | EmptyStackException e){
            // исходное выражение было не задано не верно
            return null;
        }
    }

    // определяем порядок операций, но скобки имеют наименьшее значение, для корректности алгоитма
    private static TreeMap<String, Integer> operationPriority = new TreeMap<String, Integer>();
    static {
        operationPriority.put("(", 0);
        operationPriority.put(")", 0);
        operationPriority.put("+", 1);
        operationPriority.put("-", 1);
        operationPriority.put("*", 2);
        operationPriority.put("/", 2);

    }

    // выполнение базовой операции
    public static double eval(String op, double val1, double val2) {
        if (op.equals("+")) {
            return val1 + val2;
        }
        if (op.equals("-")) {
            return val1 - val2;
        }
        if (op.equals("/")) {
            return val1 / val2;
        }
        if (op.equals("*")) {
            return val1 * val2;
        }
        throw new IllegalArgumentException("Invalid operator");
    }
}