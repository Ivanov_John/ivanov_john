package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public static int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.isEmpty()) {
            throw new CannotBuildPyramidException();
        }

        int a = 1; // минимальное необходимое кол-во элементов
        int stepNumber = 0; // номер элемента прогрессии
        int pyramidElementsCount = a; // значение эл-та прогрессии с текущим шагом

        // проверяем что кол-во элементов достаточно для построения пирамиды (1,3,6,10...)
        while (pyramidElementsCount > 0 && pyramidElementsCount < inputNumbers.size()) {
            stepNumber++;
            pyramidElementsCount = stepNumber * (stepNumber + 1) / 2; // формула треугольночго числа
        }

        if (pyramidElementsCount != inputNumbers.size()) { // не можем построить пирамиду для такого числа элементов
            throw new CannotBuildPyramidException();
        }

        // считаем количсетво элементов в строке пирамиды
        int pyramidStep = 2; // запись элементов через один
        int pyramidLength = a + (stepNumber - 1) * pyramidStep; // длина строки в пирамиде
        int result[][] = new int[stepNumber][pyramidLength];

        // сортируем последовательность
        Collections.sort(inputNumbers);

        // заполняем пирамиду
        int insert_step = 1; // сдвиг для позиции по столбцу пирамиды при переходе к след строке
        int curStepNumber = 1; // текущая строка пирамиды
        int curIndex = pyramidLength / 2; // текущий столбец с которого начинаем запись в строке

        Iterator<Integer> iter = inputNumbers.iterator();
        while (iter.hasNext()) {
            for (int i = 0; i < curStepNumber; i++) {
                result[curStepNumber - 1][curIndex] = iter.next();
                curIndex += pyramidStep;
            }
            curIndex = (curIndex - pyramidStep * curStepNumber) - insert_step;
            curStepNumber++;
        }

        return result;
    }
}